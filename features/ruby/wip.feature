Feature:

Scenario: I am able to select a date range
  Given I can run a series of command
  Then then "each" should be valid
  | x | y |
  | 1 | 2 |
  | 2 | 4 |
  | 3 | 6 |

  Given I can run a series of command
  Then then "at least one" should be valid
  | x | y |
  | 1 | 2 |
  | 2 | 4 |
  | 3 | 6 |
