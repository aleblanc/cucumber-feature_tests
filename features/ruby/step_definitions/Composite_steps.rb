=begin
Composite regroup many leaf and should always include a Given & When expression , it may contain a And expression
=end

Given(/^the board is up in linux$/) do
  steps %q{
    Given the chatter is up
    Then we can log in
  }
end

Given(/^the board is rebooting$/) do
 steps %q{
    When the line \#\s is present
    And poweroff the machine
    Then the board is up in linux
  }
end

Given(/^booting linux with (.*) timeout$/) do |time|
  steps( %Q(
    Given set chatter with "#{time}" timeout
    Then we can log in
  ))
end

Given(/^rebooting linux with (.*) timeout$/) do |timeout|
  steps( %Q(
    When the line \#\s is present
    And poweroff the machine
    Then booting linux with "#{timeout}" timeout
  ))
end

Given (/^poweroff the machine$/)do
 steps %q{
    When the line \#\s is present
    And sending command poweroff
    Then I should expect Requesting system poweroff
 }
end

Given (/^we can log in$/) do
  steps %q{
    When the line buildroot login
    And sending user root
    Then I should get linux prompt
    }
end

