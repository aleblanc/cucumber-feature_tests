require_relative '../src/uboot-chatter/Chatter'
require 'rubygems'
require 'test/unit'

=begin
dev should avoid using leaf step directly and should instead composite step
leaf step should remain as simple as possible since the require many action to be complete
=end

Given(/^the chatter is up$/) do
  @chatter = Chatter.new(DEBUG||true, TIMEOUT||50, command: COMMAND)
  @chatter.connect
  @test = {}
end

Given(/^set chatter with (.*) timeout$/) do |timeout|
  @chatter = Chatter.new(DEBUG||true, Integer(timeout.tr('\\"','')), command: COMMAND)
  @chatter.connect
  @test = {}
end

And(/^sending command (.*)$/) do |cmd|
  @test[:cmd] = cmd
end

And(/^sending user root$/) do
  @test[:cmd] = 'root'
end

When(/^the line buildroot login$/)do
  @test[:prompt] = Regexp.new(/buildroot login:\s/)
end

When(/^the line (.*) is present$/) do |regex|
  @test[:prompt] = Regexp.new(regex)
end

Then(/^I should expect (.*)$/) do |regex|
  @test[:expect] = Regexp.new(regex)
  @chatter.addTests @test
  test = @chatter.getTest
  assert_equal('ok', test[:result], "#{test}")
  @test = {}
end

Then(/^I should get linux prompt$/)do
  @test[:expect] = Regexp.new(/\#(\s)*/)
  @chatter.addTests @test
  test = @chatter.getTest
  assert_equal('ok', test[:result], "#{test}")
  @test = {}
end

Then(/^we kill everything$/) do
  @chatter.stop
end



