Feature:
  In order to test uboot
  As a boot
  I want to check it is running

  Scenario: Boot to uboot
    Given the chatter is up
    When the line Hit any key to stop autoboot:.* is present
    And sending command ' '
    Then I should expect \=\>.*
    # qemu will be shutdown using kill -9, setup in env.rb
  @mega
  Scenario: Boot to linux
    Given the chatter is up
    When the line buildroot login:  is present
    And sending command root
    Then I should expect \#\s
    #we gracefully shutdown the machine and qemu
    And poweroff the machine
  @mega
  Scenario:  boot to linux perform full shutdown and boot again , aka reboot
    # This is a macro that contain 5 steps

    Given the board is up in linux
    # An other macro
    Then the board is rebooting


