Feature:
  Check mmcblk0p3 is recovered after the corruption
  @wip @long
  Scenario: This test check that we can boot and reboot
    # This is a macro that contain 5 steps
    Given booting linux with 120 timeout

    #Test it is mounted
    When the line \#\s is present
    And sending command mount | grep /dev/mmcblk0p3
    Then I should expect /dev/mmcblk0p3 on /mnt/data type ext4 (rw,nosuid,nodev,noatime,nodiratime,commit=1,data=ordered)

    #Corrupt the partition
    When the line \#\s is present
    And sending command dd if=/dev/urandom of=/dev/mmcblk0p3 bs=1M count=10 && sync
    Then I should expect \#\s

    When the line \#\s is present
    And sending command echo $?
    Then I should expect 0

    Given rebooting linux with 120 timeout

    #Test it is mounted
    When the line \#\s is present
    And sending command mount | grep /dev/mmcblk0p3
    Then I should expect /dev/mmcblk0p3 on /mnt/data type ext4 (rw,nosuid,nodev,noatime,nodiratime,commit=1,data=ordered)